from django.db import models

# Create your models here.
# Add a simple POST class here.

# This creates the table. The model is the table. We are using Python class
# to create the table.


class Post(models.Model):  # one post
    title = models.CharField(max_length=100)
    created = models.DateTimeField()
    content = models.TextField()
    description = models.CharField(max_length=200)


class Comment(models.Model):  # many comments
    author = models.CharField(max_length=100)
    created = models.DateTimeField()
    content = models.TextField(max_length=100)

    post = models.ForeignKey(
        "Post", related_name="comments", on_delete=models.CASCADE
    )


class Keyword(models.Model):
    word = models.CharField(max_length=20)
    # ManyToMany relationship between Keyword and Post, which is held
    # in keywords in related_name
    posts = models.ManyToManyField("Post", related_name="keywords")


# Adding on.delete.Cascade causes all comments on post to get deleted, when
# post gets deleted.

# "Post" makes connection to the class Post

# related_name="comments" is what we are going to call this form key when we
# access it from other places

# on_delete indicates how we want to handle the deletion of comments, when
# we delete a post, we want all comments associated with that post to be
# deleted as well, hence .CASCADE handles that.

# post = models.ForeignKey is the method that creates that relationship
# and "Post", related_name, etc. are the parameters for that method.
