from django.urls import path

from posts.views import (
    PostListView,
    PostDetailView,
    PostUpdateView,
    PostCreateView,
)

urlpatterns = [
    path("<int:pk>/", PostDetailView.as_view(), name="post_detail"),
    path("", PostListView.as_view(), name="post_list"),
    # using our primary key to post our update = pk
    path("<int:pk>/update", PostUpdateView.as_view(), name="post_update"),
    path("create", PostCreateView.as_view(), name="post_create"),
]
