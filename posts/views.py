from django.shortcuts import render

# need to import our ListView and DetailView
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView

try:
    from posts.models import Post
except Exception:
    Post = None


# Create your views here.
# def show_post_detail(request, pk):
#    if Post:
#        post = Post.objects.get(pk=pk)
#    else:
#        post = None
#    context = {
#        "post": post,
#    }
#    return render(request, "posts/detail.html", context)


# def list_all_posts(request):
#    if Post:
#        posts = Post.objects.all()
#    else:
#        posts = None
#    context = {
#        "posts": posts,
#    }
#    return render(request, "posts/list.html", context)


# create our class view
# creating a specialized view for our post model
class PostListView(ListView):  # shows multiple posts
    model = Post
    # name our context object
    context_object = "posts"  # many posts
    # what template to display in, using our list.html file
    template_name = "posts/list.html"
    paginate_by = 3
    # queryset means the results of our query
    # self lets it know we're in "this" class
    # sends that q that was in template to request
    # if there is no query, lets assume the query is empty
    # otherwise
    # filtering request based on if the title contains  what our query is
    def get_queryset(self):
        query = self.request.GET.get("q")
        if not query:
            query = ""
        return Post.objects.filter(title_icontains=query)


# create class view
class PostDetailView(DetailView):  # shows specific post
    model = Post
    context_object_name = "post"  # in detail view, its one specific post
    template_name = "posts/detail.html"


class PostCreateView(CreateView):
    model = Post
    # which fields we want our form to be creating fields for
    fields = ["title", "content", "created_at"]
    template_name = "posts/create.html"
    success_url = "/"  # go back to index page after successfully posting


class PostUpdateView(UpdateView):
    model = Post
    fields = ["title", "content", "created_at"]
    template_name = "posts/update.html"
    success_url = "/"
